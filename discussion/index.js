// alert("hi"); flexible si JAVASCRIPT

// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Addition operator: " + sum);
//result: 9228

let difference = y - x;
console.log("Subtraction operator: " + difference);

let product = x * y;
console.log("Multiplication operator: " + product);

let quotient = x / y;
console.log("Division operator: " + quotient);

let remainder = y % x;
console.log("Modulo operator: " + remainder);
//result: 846

// Assignment Operator
	//Basic Assignment Operator (=) - assigning a certain value

let assignmentNumber = 8; //let assignmentNumber is 8

	//	Addition Assignment Operator (+=)

//assignmentNumber = assignmentNumber + 2
//assignmentNumber = 8+2, re-assignment pa rin

assignmentNumber +=2;
console.log("Addition assignment: " + assignmentNumber);
//result: 10

assignmentNumber +=2;
console.log("Addition assignment: " + assignmentNumber);
//result: 12

let string1 = "Boston";
let string2 = "Celtics";
string1 += string2;
console.log(string1);
//result: BostonCeltics
//string1 = string1 + string2

assignmentNumber -=2;
console.log("Subtraction assignment: " + assignmentNumber);
//result: 10

assignmentNumber *=2;
console.log("Multiplication assignment: " + assignmentNumber);
//result: 20

assignmentNumber /=2;
console.log("Division assignment: " + assignmentNumber);
//result: 10

// Multiple Operators and Parentheses
let mdas = 1 + 2 - 3 * 4 /5;
console.log("MDAS: " + mdas);
//result: 0.6
/*
	1. 3*4 = 12
	2. 12/5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6
*/

let pemdas = 1 + (2-3) * (4/5);
console.log("PEMDAS: " + pemdas);
//result: 0.199 or 0.2
/*
	1. 4/5 = 0.8
	2. 2-3 = -1
	3. -1 * 0.8 = -0.8
	4. 1 + -0.8 = 0.2

*/

// Increment and Decrement
let z = 1;

let increment =++z; //add agad and reassign
console.log("Pre-increment: " + increment);
//result: 2
console.log("Value of z: " + z);
//result: Value of z: 2 (string + value of z)

increment = z++;
console.log("Post-increment: " + increment);
//result: 2 (nilalabas nya muna yung current value, wala pang one)balik ko muna syao ang curent value bago magadd
console.log("Value of z: " + z);
//result: 3, mas madalas

//let array = [1, 2, 3]
//console.log(1, 2, 3)

let decrement = --z;
console.log("Pre-decrement: " + decrement);
//result: 2
console.log("Value of z:" + z);
//result: 2

decrement = z--;
console.log ("Post-decrement: " + decrement);
//result: 2
console.log("Value of z: " + z);
//result: 1

//Type Coercion (pagconvert ng data type implicitly, behind)
let numA = '10';
let numB = 12;
let coercion = numA + numB;
console.log(coercion); //result: 1012
console.log(typeof coercion); //result: string

let numC = 16;
let numD = 14;
let nonCoercioon = numC + numD;
console.log(nonCoercioon); //result: 30
console.log(typeof nonCoercioon); //result: number

let numE = true + 1; //true is = 1
console.log(numE); //result: 2

let numF = false + 1; //false is = 0
console.log(numF); //result: 1

// let numG = false + true;
// console.log(numG);

//Equality Operator (==) -- result either true or false

console.log(1 == 1);//result: true (boolean)
console.log(1 == 2);//result: false
console.log(1 == '1');//result: true -- coercion gumagana 
console.log(false == 0); //result: true
console.log('johnny' == 'johnny') //result: true
console.log('Johnny' == 'johnny') //result: 

//Inequality Operator (!=), testing if not equal

console.log(1 != 1); //result: false
console.log(1 != 2); //result: true
console.log(1 != '1');//result: false
console.log(0 != false); //result: false
console.log('johnny' != 'johnny'); // result: false
console.log('Johnny' != 'johnny'); //result: true

//Strict Equality Operator
console.log(1 === 1);//result: true same value and data type
console.log(1 === '1');//result: false kase strict, pati data type 
console.log('johnny' === 'johnny'); //result: true

let johnny = 'johnny';
console.log('johnny' === johnny); //result: true

// let johnny1 = '1';
// console.log('1' === johnny1) //result: true

console.log (false === 0); //result: false

//Strict Inequality Operator (!==)
console.log(1 !== 1); //result: false
console.log(1 !== 2); //result: true
console.log(1 !== '1'); //result: true
console.log('johnny' !== johnny); //false

// Relational Operator ><<>

let a = 50;
let b = 65;

//GT or Greater Than (>)
let isGreaterThan = a>b; //a is not greater than b therefore false
console.log(isGreaterThan); //result: false

//LT or Less Than (<)
let isLessThan = a<b;
console.log(isLessThan); //result: true

//GTE or Greater Tham or Equal (>=)
let isGTE = a>=b;
console.log(isGTE); //result: false

//LTE or Less Than or Equal (<=)
let isLTE = a<=b;
console.log(isLTE); //result: true

//forced coercion to change the string to a number.

let numStr = '30';
console.log(a>numStr); //result: true

let str = 'twenty'; //kasi string si twenty
console.log(b>=str); //result:false



//Logical Operators
	//AND Operator (&&) pag parehong true, true

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1); //result: false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2); //result: true

let authorization3 = isAdmin && isLegalAge;
console.log(authorization3); //result: false

let random = isAdmin && false;
console.log(random); //result: false

let requiredLevel = 95;
let requiredAge = 18;

let authorization4 = isRegistered && requiredLevel === 25;
console.log(authorization4); //result: false

let authorization5 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization5); //result: true

let userName = 'gamer2022'; //nagsisimula sa 1
let userName2 = 'the Tinker';
let userAge = 15;
let userAge2 = 26;

let registration1 = userName.length > 8 && userAge >=requiredAge;
console.log(registration1); //result: false

let registration2 = userName.length > 8 && userAge2 >=requiredAge;
console.log(registration2); //result:true



	//OR Operator (|| - Double Pipe) pag may isang true, true na

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
console.log(guildRequirement); //result: false

let guildRequirement2 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement2); //result: true

let guildRequirement3 = userLevel >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement3); //result: true

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); //result:false

// Not Operator (!) --- binabaliktad yung boolean value

console.log(!isRegistered); //result: false

let opposite1 = !isAdmin;
console.log(opposite1); //result: true

